# Serverless - Frontend

Serverless single page app frontend framework

### Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* node v10.0.0
* yarn v1.6.0
* watchman

## Installing - Development

```
$ git clone ...
$ cd clone
$ touch .env
$ NODE_ENV=developemt > .env
$ yarn install
$ yarn start
```

## .env
`.env` needs to be filled with the following:

```text
NODE_ENV=[production|development]
API_ENDPOINT=...
GRAPHQL_ENDPOINT=...
```

## Tests

*tbc*

## Coding style
All style rules are defined within `tslint.json`.

```
$ yarn lint
```
*NB* - Running `yarn start` will flag style errors.

```
// For Webstorm set up
Preferences > Tools > File Watchers // Add prettier: one for .ts and another for .tsx
```

## Directory Structure
All code lives in `src/` directory and within this you will following structure:
```
.
|-- src/
|   |-- app/
|   |   |-- public/
|   |   |   |-- landing/
|   |   |-- protected/
|   |   |   |-- dashboard/
|   |   |-- index.tsx
|   |-- components/
|   |-- utils/
|   |-- theme/
|   |-- utils/
|   |-- index.tsx
```

Within the `app/` directory there are two sub directories that host `public` or `protected` routes. All protected routes will be `lazy loaded` on authorisation.

Both route directories follows the `feature-first` structure and should therefore have all its sub directories split into and named after the feature and/or `route`.

```
/protected
|   |-- index.tsx
|   |-- home/
|   |-- dashboard/
...
```

Each feature will then follow the [duck](https://medium.freecodecamp.org/scaling-your-redux-app-with-ducks-6115955638be) directory structure.

```
dashboard/
|   |-- dashboard.Coponent.tsx
|   |-- dashboard.Container.tsx
|   |-- components/
|   |   |-- header.ts
|   |-- assets/
|   |-- duck/
|   |   |-- index.ts
|   |   |-- actions.ts
|   |   |-- operations.ts
|   |   |-- reducers.ts
|   |   |-- selectors.ts
|   |   |-- tests.ts
|   |   |-- types.ts
...
```

Components should be split between `presentational` and `containers` with presentation looking after `ui` and the container wrapping the presentation and maintaining data structures and passing the data down via `props`. `components` directory containts all dummy components for the route.

Lastly functional componants are stored in the `components` eg

```
components/
|   |-- Button/
|   |   |-- index.tsx
```

## Styling
All styling must conform to the theme set out inside `theme/` directory utilising the colours/typography/trasitions etc.

Within the `theme` directory you will find the following dummy structure:

```
/theme
|   |-- index.ts
|   |-- palette.ts
|   |-- transitions.ts
|   |-- border.ts
|   |-- typography.ts
|   |-- colors/
|   |   |-- index.ts
|   |   |-- blue.ts
|   |   |-- common.ts
|   |   |-- grey.ts
|   |   |-- red.ts
```

### Jss vs inline
Styling components can be acheived by either `inline` styles and using `jss`. The apporopriate choice depends on requirments of the components but a rule of thumb is: if a `presentational` component then use jss and if a componet that requires styles to be passed via `props`, i.e. `button`, then use `inline`

### react-jss
*NB `react-jss` does not have any offcial `@types` - therefore a custom has been created*

when using `jss` within the application we use the `hoc` `injectSheet` from [react-jss](https://github.com/cssinjs/react-jss). Theme is injected in the route of the application and can be access by passing a function with a `theme` arguement, otherwise `injectSheet` also accepts a plain `object`

```
import React from 'react'
import injectSheet from 'react-jss'

// without theme
const styles = {
  button: {
    color: 'green'
  }
}

// with theme
const themeStyle = theme => ({
    button: {
        color: theme.primaryColor1
    }
})

const Button = ({classes}) => {
  return <button className={classes.button}>My Button</button>
}

export default injectSheet(styles)(Button)
```

If you need access to the theme you can use `withTheme`

```
import React from 'react'
import {withTheme} from 'react-jss'

const Button = ({theme}) => {
  return <div>{`My color is ${theme.color}`}</vi>
}

export default withTheme(Button)
```

### Assets

In general all assets should live in their respective view

```
login/
|   |-- duck/
|   |-- assets/
|   |   |-- image.png
|   |   |-- logo.svg
|   |-- Login.Coponent.tsx
|   |-- Login.Container.tsx
...
```

Rember to add any rules to webpack.

However, for any static assets; `favicon/fonts` etc then these can be saved in the root `assets/` directory. From here they can be added via the `index.html`

## Deployment

*tbc*

##JQuery

*`jquery` must not be used within project.*

## Favicons

Favicons are generated using `favicons-webpack-plugin` so simply need to place `png` in location and reference in `webpack`

*NOTE*

When building the plugin doesnt have an ouput option. I have shimmed the package in `node_modules` so you need to change the following line:

```javascript
/*
* node_modules/favicons-webpack-plugin/lib/favicons.js
* The following line:
* return entry.replace(/(href=[""])/g, '$1' + publicPath + pathPrefix));
* should be replace with
* return entry.replace(/(href=[""])/g, '$1' + path.join(publicPath, pathPrefix));
* 
* And require path at the top of the file.
* The fucntion should look like the following
* */

function generateIcons (loader, imageFileStream, pathPrefix, query, callback) {
  var publicPath = getPublicPath(loader._compilation);
  favicons(imageFileStream, {
    path: '',
    url: '',
    icons: query.icons,
    background: query.background,
    appName: query.appName
  }, function (err, result) {
    if (err) return callback(err);
    var html = result.html.filter(function (entry) {
      return entry.indexOf('manifest') === -1;
    })
    .map(function (entry) {
        // THIS LINE HERE
      return entry.replace(/(href=[""])/g, '$1' + path.join(publicPath, pathPrefix));
    });
    var loaderResult = {
      outputFilePrefix: pathPrefix,
      html: html,
      files: []
    };
    result.images.forEach(function (image) {
      loaderResult.files.push(pathPrefix + image.name);
      loader.emitFile(pathPrefix + image.name, image.contents);
    });
    result.files.forEach(function (file) {
      loaderResult.files.push(pathPrefix + file.name);
      loader.emitFile(pathPrefix + file.name, file.contents);
    });
    callback(null, loaderResult);
  });
}
```

## Typescript

All `type` declarations sit in `@types/`

## Built With

* [Typescript](https://www.typescriptlang.org/)
* [React](https://reactjs.org/) - Frontend
* [Redux](https://github.com/reactjs/redux) - State mangement
* [react-jss](http://cssinjs.org/) - Styles, see abstractions
* [React Router](https://github.com/ReactTraining/react-router)

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **Alan Williams**
