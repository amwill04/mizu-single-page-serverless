import * as React from 'react';
import palette from './palette';

export type TextStyle =
    | 'display1'
    | 'display2'
    | 'display3'
    | 'display4'
    | 'headline'
    | 'title'
    | 'subheading'
    | 'body1'
    | 'body2'
    | 'caption';

export type Style = TextStyle | 'button';

interface FontStyle {
    fontFamily: React.CSSProperties['fontFamily'];
    fontSize: number;
    fontWeightLight: React.CSSProperties['fontWeight'];
    fontWeightRegular: React.CSSProperties['fontWeight'];
    fontWeightMedium: React.CSSProperties['fontWeight'];
    htmlFontSize?: number;
}

export interface TypographyStyle {
    color?: React.CSSProperties['color'];
    fontFamily: React.CSSProperties['fontFamily'];
    fontSize: React.CSSProperties['fontSize'];
    fontWeight: React.CSSProperties['fontWeight'];
    letterSpacing?: React.CSSProperties['letterSpacing'];
    lineHeight?: React.CSSProperties['lineHeight'];
    textTransform?: React.CSSProperties['textTransform'];
}

export interface TypographyUtils {
    pxToRem: (px: number) => string;
}

export type Typography = Partial<Record<Style, TypographyStyle>> & FontStyle & TypographyUtils;

const coef: number = 14 / 14;
const pxToRem = (value: number): string => {
    return `${(value / 16) * coef}rem`;
};

const round = (value: number): number => {
    return Math.round(value * 1e5) / 1e5;
};

const fontFamily = 'Quicksand, sans-serif';
const fontWeightLight = 300;
const fontWeightMedium = 700;
const fontWeightRegular = 400;
const fontSize = 14;
const htmlFontSize = 16;

/* tslint:disable:object-literal-sort-keys */
const typography: Typography = {
    fontFamily,
    fontSize,
    fontWeightLight,
    fontWeightMedium,
    fontWeightRegular,
    htmlFontSize,
    pxToRem,
    display4: {
        color: palette.text.secondary,
        fontFamily,
        fontSize: pxToRem(112),
        fontWeight: fontWeightLight,
        letterSpacing: '0.3rem',
        lineHeight: `${round(128 / 112)}em`,
    },
    display3: {
        color: palette.text.secondary,
        fontFamily,
        fontSize: pxToRem(56),
        fontWeight: fontWeightRegular,
        letterSpacing: '0.3rem',
        lineHeight: `${round(73 / 56)}em`,
    },
    display2: {
        color: palette.text.secondary,
        fontFamily,
        fontSize: pxToRem(45),
        fontWeight: fontWeightRegular,
        lineHeight: `${round(48 / 45)}em`,
    },
    display1: {
        color: palette.text.secondary,
        fontFamily,
        fontSize: pxToRem(34),
        fontWeight: fontWeightRegular,
        lineHeight: `${round(41 / 34)}em`,
    },
    headline: {
        color: palette.text.primary,
        fontFamily,
        fontSize: pxToRem(24),
        fontWeight: fontWeightRegular,
        lineHeight: `${round(32.5 / 24)}em`,
    },
    title: {
        color: palette.text.primary,
        fontFamily,
        fontSize: pxToRem(20),
        fontWeight: fontWeightRegular,
        lineHeight: `${round(24.5 / 21)}em`,
    },
    subheading: {
        color: palette.text.primary,
        fontFamily,
        fontSize: pxToRem(16),
        fontWeight: fontWeightRegular,
        lineHeight: `${round(24 / 16)}em`,
    },
    body2: {
        color: palette.text.primary,
        fontFamily,
        fontSize: pxToRem(14),
        fontWeight: fontWeightMedium,
        lineHeight: `${round(24 / 14)}em`,
    },
    body1: {
        color: palette.text.primary,
        fontFamily,
        fontSize: pxToRem(14),
        fontWeight: fontWeightRegular,
        lineHeight: `${round(20.5 / 14)}em`,
    },
    caption: {
        color: palette.text.secondary,
        fontFamily,
        fontSize: pxToRem(12),
        fontWeight: fontWeightRegular,
        lineHeight: `${round(16.5 / 12)}em`,
    },
    button: {
        color: palette.text.primary,
        fontFamily,
        fontSize: pxToRem(14),
        fontWeight: fontWeightMedium,
        textTransform: 'uppercase',
    },
};

export default typography;
