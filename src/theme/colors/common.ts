type Colors = 'black' | 'white';

export type CommonColors = Record<Colors, string>;

const common: CommonColors = {
    black: '#000',
    white: '#fff',
};

export default common;
