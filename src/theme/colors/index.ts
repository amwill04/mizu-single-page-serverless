export { default as grey } from './grey';
export { default as blue } from './blue';
export { default as red } from './red';
export { default as common } from './common';
export interface ColorPallete {
    50: string;
    100: string;
    150: string;
    200: string;
    250: string;
}
