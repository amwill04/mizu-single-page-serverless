const grey = {
    50: '#F7F7F7',
    100: '#D6D6D6',
    150: '#B0B0B0',
    200: '#8A8A8A',
    250: '#3B3B3B',
};

export default grey;
