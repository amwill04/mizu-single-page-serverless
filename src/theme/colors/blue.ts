const blue = {
    50: '#79ABDC',
    100: '#56799C',
    150: '#33485D',
    200: '#2C3E50',
    250: '#1E2A36',
};

export default blue;
