import { grey } from './colors';

export interface CreateBorder {
    width: number;
    color: string;
    style?: string;
    radius?: string | number;
}

export interface BordersUtils {
    createBorder: (width: number, color: string, style?: string, radius?: string | number | null) => CreateBorder;
}

type BorderTypes = 'default' | 'light' | 'thick' | 'lightSoft';

export type Borders = Record<BorderTypes, CreateBorder> & BordersUtils;

const createBorder = (
    width: number,
    color: string,
    style: string = 'solid',
    radius: string | number | null = null
): CreateBorder => ({
    color,
    style,
    width,
    ...(radius && { radius }),
});

const borders: Borders = {
    createBorder,
    default: createBorder(2, grey[100], 'solid'),
    light: createBorder(2, grey[50], 'solid'),
    lightSoft: createBorder(2, grey[50], 'solid', 4),
    thick: createBorder(10, grey[100], 'solid'),
};

export default borders;
