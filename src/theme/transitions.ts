interface Easing {
    easeIn: string;
    easeInOut: string;
    easeOut: string;
    sharp: string;
}

interface Duration {
    complex: number;
    enteringScreen: number;
    leavingScreen: number;
    short: number;
    shorter: number;
    shortest: number;
    standard: number;
}

export interface Transitions {
    easing: Easing;
    duration: Duration;
}

export const easing: Easing = {
    easeIn: 'cubic-bezier(0.4, 0, 1, 1)',
    easeInOut: 'cubic-bezier(0.4, 0, 0.2, 1)',
    easeOut: 'cubic-bezier(0.0, 0, 0.2, 1)',
    sharp: 'cubic-bezier(0.4, 0, 0.6, 1)',
};

export const duration: Duration = {
    complex: 375,
    enteringScreen: 225,
    leavingScreen: 195,
    short: 250,
    shorter: 200,
    shortest: 150,
    standard: 300,
};

const transitions: Transitions = {
    duration,
    easing,
};

export default transitions;
