import borders, { Borders } from './borders';
import palette, { Palete } from './palette';
import shadows, { Shadows } from './shadows';
import transitions, { Transitions } from './transitions';
import typography, { Typography } from './typography';

export interface Theme {
    palette: Palete;
    transitions: Transitions;
    typography: Typography;
    borders: Borders;
    shadows: Shadows;
}

const theme: Theme = {
    borders,
    palette,
    shadows,
    transitions,
    typography,
};

export default theme;
