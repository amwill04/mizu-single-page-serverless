import { blue, ColorPallete, common, grey, red } from './colors';
import { CommonColors } from './colors/common';

interface ActionTheme {
    active: string;
    disabled: string;
    disabledBackground: string;
    hover: string;
    hoverOpacity: number;
    selected: string;
}

interface BackgroundTheme {
    default: string;
    paper: string;
}

interface TexdTheme {
    disabled: string;
    hint: string;
    primary: string;
    secondary: string;
}

interface ColorTheme {
    dark: string;
    light: string;
    main: string;
}

type colors = 'red' | 'blue' | 'grey';

export interface Palete {
    colors: Record<colors, ColorPallete>;
    common: CommonColors;
    primary: ColorTheme;
    error: ColorTheme;
    action: ActionTheme;
    background: BackgroundTheme;
    text: TexdTheme;
}

const action: ActionTheme = {
    active: 'rgba(0, 0, 0, 0.54)',
    disabled: 'rgba(0, 0, 0, 0.26)',
    disabledBackground: 'rgba(0, 0, 0, 0.12)',
    hover: 'rgba(0, 0, 0, 0.08)',
    hoverOpacity: 0.08,
    selected: 'rgba(0, 0, 0, 0.14)',
};
const background: BackgroundTheme = {
    default: grey[50],
    paper: common.white,
};
const text: TexdTheme = {
    disabled: 'rgba(0, 0, 0, 0.38)',
    hint: 'rgba(0, 0, 0, 0.38)',
    primary: 'rgba(0, 0, 0, 0.87)',
    secondary: 'rgba(0, 0, 0, 0.54)',
};

const primary: ColorTheme = {
    dark: blue[250],
    light: blue[50],
    main: blue[150],
};

const error: ColorTheme = {
    dark: red[250],
    light: red[50],
    main: red[150],
};

/* tslint:disable:object-literal-sort-keys */
const palette: Palete = {
    colors: {
        grey,
        red,
        blue,
    },
    common,
    primary,
    error,
    action,
    background,
    text,
};

export default palette;
