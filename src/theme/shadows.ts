import { grey } from './colors';

export interface Shadow {
    x: number;
    y: number;
    blur: number;
    spread: number;
    color: string;
}

type ShadowTypes = 'soft';

const createShadow = (x: number, y: number, blur: number, spread: number, color: string): Shadow => ({
    blur,
    color,
    spread,
    x,
    y,
});

interface Utils {
    createShadow: (x: number, y: number, blur: number, spread: number, color: string) => Shadow;
}

export type Shadows = Record<ShadowTypes, Shadow> & Utils;

const shadows: Shadows = {
    createShadow,
    soft: createShadow(0, 4, 25, 0, grey['250']),
};

export default shadows;
