// TODO Check to see if #credentials option can change to 'same-origin' on production
import { isProduction } from './index';

const fetchOptions: RequestInit = {
    credentials: isProduction ? 'same-origin' : 'include',
};

const fetchError = (response: Response): Response | Error => {
    switch (response.status) {
        case 422: {
            return response;
        }
        default: {
            throw new Error(response.statusText);
        }
    }
};

export const fetchFormData = async (endpoint: string, formData: { [k: string]: any }): Promise<Response> => {
    const formBody = Object.keys(formData)
        .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(formData[key]))
        .join('&');
    const options = {
        ...fetchOptions,
        body: formBody,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        },
        method: 'POST',
    };
    return fetch(endpoint, options).then((response: Response) => {
        if (!response.ok) {
            fetchError(response);
        }
        return response;
    });
};

export const fetchPage = async (endpoint: string): Promise<Response> => {
    return fetch(endpoint, fetchOptions).then((response: Response) => {
        if (!response.ok) {
            fetchError(response);
        }
        return response;
    });
};
