export { fetchFormData } from './fetch';

export const isProduction = process.env.NODE_ENV === 'production';

export const getDocHeight = () => {
    const D = document;
    return Math.max(
        D.body.scrollHeight,
        D.documentElement.scrollHeight,
        D.body.offsetHeight,
        D.documentElement.offsetHeight,
        D.body.clientHeight,
        D.documentElement.clientHeight
    );
};
