import { HomeActions } from 'duck/protected/Home';
import { StateType } from 'typesafe-actions';
import rootReducer from './rootReducer';

export type RootState = StateType<typeof rootReducer>;
export type RootAction = HomeActions;

export { default } from './store';
