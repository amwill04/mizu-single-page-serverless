import { homeReducer } from 'duck/protected/Home';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    home: homeReducer,
});

export default rootReducer;
