import { homeSagas } from 'duck/protected/Home';
import { all } from 'redux-saga/effects';

export default function* rootSaga() {
    yield all([...homeSagas]);
}
