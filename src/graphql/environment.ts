import { Environment, FetchFunction, Network, RecordSource, Store } from 'relay-runtime';

const fetchQuery: FetchFunction = (operation, variables) =>
    fetch(process.env.API_ENDPOINT, {
        body: JSON.stringify({
            query: operation.text,
            variables,
        }),
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
        },
        method: 'POST',
    }).then(response => response.json());

const network = Network.create(fetchQuery);
const store = new Store(new RecordSource());

const environment = new Environment({
    network,
    store,
});

export default environment;
