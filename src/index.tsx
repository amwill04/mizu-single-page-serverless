import 'normalize.css';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';

const renderApp = (Component: React.ComponentType) => ReactDOM.render(<Component />, document.getElementById('app'));

renderApp(App);

if ((module as any).hot) {
    (module as any).hot.accept('./app/index', () => {
        const NextApp = require('./app').default;
        renderApp(NextApp);
    });
}
