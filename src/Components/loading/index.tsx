import React from 'react';
import injectSheet from 'react-jss';
import { Theme } from 'theme';

type ClassNames = 'root' | 'loader' | 'circular' | 'path';

interface LoaderProps {
    classes: Record<ClassNames, string>;
}

/* tslint:disable:object-literal-sort-keys */
const styles = (theme: Theme) => ({
    root: {
        width: '100%',
        height: '100%',
        backgroundColor: theme.palette.background.default,
    },
    loader: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        margin: {
            top: -50,
            right: 0,
            bottom: 0,
            left: -50,
        },
        width: 100,
        height: 100,
    },
    circular: {
        animation: {
            name: 'rotate',
            duration: 2000,
            timingFunction: 'linear',
            iterationCount: 'infinite',
        },
        height: '100%',
        transformOrigin: 'center center',
        width: '100%',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        margin: 'auto',
    },
    path: {
        strokeDasharray: '1, 200',
        strokeDashoffset: 0,
        animation: 'dash 1.5s ease-in-out infinite, color 6s ease-in-out infinite',
        strokeLinecap: 'round',
    },
    '@keyframes rotate': {
        '100%': {
            transform: 'rotate(360deg)',
        },
    },
    '@keyframes dash': {
        '0%': {
            strokeDasharray: '1, 200',
            strokeDashoffset: 0,
        },
        '50%': {
            strokeDasharray: '89, 200',
            strokeDashoffset: -35,
        },
        '100%': {
            strokeDasharray: '89, 200',
            strokeDashoffset: -124,
        },
    },
    '@keyframes color': {
        '0%': {
            stroke: theme.palette.primary.light,
        },
        '50%': {
            stroke: theme.palette.primary.main,
        },
        '100%': {
            stroke: theme.palette.primary.dark,
        },
    },
});
/* tslint:enable:object-literal-sort-keys */

const Loading = ({ classes }: LoaderProps) => {
    const { loader, circular, path, root } = classes;
    return (
        <div className={root}>
            <div className={loader}>
                <svg className={circular} viewBox="25 25 50 50">
                    <circle className={path} cx="50" cy="50" r="20" fill="none" strokeWidth="2" strokeMiterlimit="10" />
                </svg>
            </div>
        </div>
    );
};

export default injectSheet(styles)(Loading);
