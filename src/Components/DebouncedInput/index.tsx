/**
 * See https://github.com/nkbt/react-debounce-input
 * Simplified version
 * https://css-tricks.com/debouncing-throttling-explained-examples/ for reference
 */
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import debounce from 'lodash/debounce';
import React, { SyntheticEvent } from 'react';
import injectSheet, { Styles } from 'react-jss';
import { Theme } from 'theme';

interface DebounceInputProps {
    classes: Record<ClassNames, string>;
    debounceTimeout: number;
    onChange: (event: Event) => void;
    placeholder?: string;
    value?: string;
}

interface DebounceInputState {
    isFocused: boolean;
    value: string;
}

type ClassNames = 'input' | 'root' | 'searchIcon' | 'searchIconFocus';

const styles = (theme: Theme): Styles<ClassNames> => ({
    input: {
        '&:focus': {
            borderColor: theme.palette.primary.light,
            outline: 0,
            padding: [10, 5, 10, 10],
        },
        border: theme.borders.lightSoft,
        color: theme.palette.text.hint,
        display: 'block',
        padding: [10, 5, 10, 40],
        transition: {
            duration: 200,
            timeingFunction: theme.transitions.easing.easeOut,
        },
        width: '100%',
    },
    root: {
        margin: '15px auto',
        position: 'relative',
        width: '90%',
    },
    searchIcon: {
        color: theme.palette.text.hint,
        display: 'block',
        fontSize: 20,
        left: 10,
        position: 'absolute',
        top: 10,
    },
    searchIconFocus: {
        extend: 'searchIcon',
        opacity: 0,
        transition: {
            duration: 200,
            property: 'opacity',
            timingFunction: theme.transitions.easing.easeOut,
        },
    },
});

class DebouncedInput extends React.Component<DebounceInputProps, DebounceInputState> {
    constructor(props: DebounceInputProps) {
        super(props);
        this.state = {
            isFocused: false,
            value: props.value || '',
        };
    }
    private _debouce: any;

    private _createDebounce = (debounceTimeout: number) => {
        if (debounceTimeout < 0) {
            this._debouce = (): void => undefined;
            return;
        }
        if (debounceTimeout === 0) {
            this._debouce = this._doOnchange;
            return;
        }
        const debounceChangedFunc = debounce((event: Event) => {
            this._doOnchange(event);
        }, debounceTimeout);
        this._debouce = (event: Event) => debounceChangedFunc(event);
    };

    private _doOnchange = (event: Event) => {
        const { onChange } = this.props;
        onChange(event);
    };

    private _onChange = (event: SyntheticEvent<HTMLInputElement>) => {
        event.persist();
        this.setState({ value: event.currentTarget.value }, () => {
            this._debouce(event);
        });
    };

    private _onFocus = () => {
        this.setState((prevState: DebounceInputState) => ({
            isFocused: !prevState.isFocused,
        }));
    };

    public static defaultProps = {
        placeholder: 'Search',
    };

    public componentDidMount() {
        this._createDebounce(this.props.debounceTimeout);
    }
    // Disable trainling comma due to tslint bug see https://github.com/palantir/tslint/issues/3960
    /* tslint:disable:trailing-comma */
    public render() {
        const {
            classes: { input, root, searchIcon, searchIconFocus },
            value: _value,
            onChange: _onChange,
            debounceTimeout: _debounceTimeout,
            ...props
        } = this.props;
        const { isFocused } = this.state;
        return (
            <div className={root}>
                <input
                    className={input}
                    {...props}
                    onChange={this._onChange}
                    value={this.state.value}
                    onBlur={this._onFocus}
                    onFocus={this._onFocus}
                />
                <span className={isFocused ? searchIconFocus : searchIcon}>
                    <FontAwesomeIcon icon={faSearch} />
                </span>
            </div>
        );
    }
}

export default injectSheet(styles)(DebouncedInput);
