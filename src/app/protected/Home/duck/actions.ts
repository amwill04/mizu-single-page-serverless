import { createAsyncAction } from 'typesafe-actions';
import { Header } from './models';
import { FETCH_DATA, FETCH_DATA_ERROR, FETCH_DATA_SUCCESS } from './types';

export const fetchData = createAsyncAction(FETCH_DATA, FETCH_DATA_SUCCESS, FETCH_DATA_ERROR)<number, Header, void>();
