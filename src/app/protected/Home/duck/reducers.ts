import { combineReducers } from 'redux';
import { ActionType } from 'typesafe-actions';
import * as actions from './actions';
import { Header } from './models';
import { FETCH_DATA_SUCCESS } from './types';

export interface HomeState {
    readonly header: Header;
}

export type HomeActions = ActionType<typeof actions>;

const headerInitialState: Header = {
    completed: null,
    pending: null,
    total: null,
};

export default combineReducers<HomeState, HomeActions>({
    header: (state = headerInitialState, action) => {
        switch (action.type) {
            case FETCH_DATA_SUCCESS: {
                return action.payload;
            }
            default: {
                return state;
            }
        }
    },
});
