import { Header } from './models';
import { HomeState } from './reducers';

export const getHomeHeader = (state: HomeState): Header => state.header;
