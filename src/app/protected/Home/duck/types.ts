export const FETCH_DATA_SUCCESS = 'app/home/FETCH_HOME_DATA/SUCCESS';
export const FETCH_DATA = 'app/home/FETCH_HOME_DATA';
export const FETCH_DATA_ERROR = 'app/home/FETCH_HOME_DATA/ERROR';
