import { call, put, takeLatest } from 'redux-saga/effects';
import { ActionType } from 'typesafe-actions';
import * as actions from './actions';
import { FETCH_DATA } from './types';

const fakeAPI = (_userId: number) =>
    new Promise(resolve => {
        setTimeout(() => resolve({ total: 200, completed: 193, pending: 7 }), 2000);
    });

function* fetchData(action: ActionType<typeof actions.fetchData.request>) {
    try {
        const data = yield call(fakeAPI, action.payload);
        return yield put(actions.fetchData.success(data));
    } catch (error) {
        yield put(actions.fetchData.failure());
    }
}

export const homeSagas = [takeLatest(FETCH_DATA, fetchData)];
