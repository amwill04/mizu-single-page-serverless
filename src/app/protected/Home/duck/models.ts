export interface Header {
    completed: number | null;
    total: number | null;
    pending: number | null;
}
