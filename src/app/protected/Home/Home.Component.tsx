import React from 'react';
import injectSheet from 'react-jss';
import { Theme } from 'theme';
import CampaignBanner from './components/CampaignBanner';
import Header from './components/Header';
import Users from './components/Users/index';

type ClassNames = 'root';

export interface HomeProps {
    classes: Record<ClassNames, string>;
}

const styles = (theme: Theme) => ({
    root: {
        backgroundColor: theme.palette.background.default,
        height: '100%',
        overflow: 'auto',
    },
});

class HomeComponent extends React.Component<HomeProps, {}> {
    public render() {
        const { classes: { root } } = this.props;
        return (
            <div className={root}>
                <Header />
                <CampaignBanner />
                <Users />
            </div>
        );
    }
}

export default injectSheet(styles)(HomeComponent);
