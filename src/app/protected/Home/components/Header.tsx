import React from 'react';
import injectSheet from 'react-jss';
import { Theme } from 'theme';
import homeVideo from '../assets/home.mp4';
import homeLogo from '../assets/logo.svg';

type ClassNames = 'banner' | 'logo' | 'logoBanner' | 'video' | 'videoContainer';

export interface HeaderProps {
    classes: Record<ClassNames, string>;
}

const styles = (theme: Theme) => ({
    banner: {
        height: '95vh',
        maxHeight: 700,
        overflow: 'hidden',
        paddingTop: 100,
        position: 'relative',
    },
    logo: {
        boxShadow: theme.shadows.soft,
    },
    logoBanner: {
        position: 'relative',
        textAlign: 'center',
        top: '-50px',
        zIndex: 2,
    },
    video: {
        '@media (max-width: 1000px)': {
            height: '100%',
            width: 'auto',
        },
        left: '50%',
        position: 'absolute',
        top: 0,
        transform: 'translateX(-50%)',
        width: '150%',
    },
    videoContainer: {
        height: '100%',
        left: 0,
        overflow: 'hidden',
        position: 'absolute',
        top: 0,
        width: '100%',
    },
});

const Header: React.SFC<HeaderProps> = ({ classes: { banner, logo, logoBanner, videoContainer, video } }) => (
    <>
        <div className={banner}>
            <div className={videoContainer}>
                <video className={video} autoPlay={true} muted={true} loop={true}>
                    <source src={homeVideo} type="video/mp4" />
                </video>
            </div>
        </div>
        <div className={logoBanner}>
            <img className={logo} src={homeLogo} height={200} />
        </div>
    </>
);

export default injectSheet(styles)(Header);
