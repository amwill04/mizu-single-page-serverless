import { homeActions, homeSelectors } from 'duck/Home';
import React from 'react';
import injectSheet, { Styles } from 'react-jss';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { RootState } from 'store';
import { Theme } from 'theme';
import InfoWidget from './CampaignInfo';

type ClassNames = 'root';

interface CamapaignBannerProps {
    classes: Record<ClassNames, string>;
    completed: number;
    total: number;
    pending: number;
    fetchData: (userId: number) => any;
}

const styles: Styles = (theme: Theme) => ({
    root: {
        '@media (max-width: 800px)': {
            alignItems: 'flex-start',
            flexDirection: 'column',
            flexShrink: 0,
            height: 430,
        },
        alignItems: 'center',
        backgroundColor: theme.palette.background.paper,
        display: 'flex',
        flexShrink: 0,
        height: 150,
        justifyContent: 'space-around',
        margin: {
            bottom: 50,
            top: 50,
        },
        padding: [20, 100],
        width: '100%',
    },
});

class CampaignBanner extends React.Component<CamapaignBannerProps, {}> {
    public componentDidMount() {
        const { fetchData } = this.props;
        fetchData(10);
    }

    public render() {
        const { classes: { root }, completed, pending, total } = this.props;
        return (
            <div className={root}>
                <InfoWidget value={completed} type={'completed'} />
                <InfoWidget value={pending} type={'pending'} />
                <InfoWidget value={total} type={'total'} />
            </div>
        );
    }
}

const mapStateToProps = (state: RootState) => homeSelectors.getHomeHeader(state.home);

const mapDispatchToProps = (dispatch: Dispatch) => ({
    fetchData: (userId: number) => dispatch(homeActions.fetchData.request(userId)),
});

const Component = injectSheet(styles)(CampaignBanner);

export default connect(mapStateToProps, mapDispatchToProps)(Component);
