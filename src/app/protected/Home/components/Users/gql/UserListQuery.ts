import { graphql } from 'react-relay';

export default graphql`
    query UserListQuery($input: UserListInput!, $first: Int, $after: String) {
        ...UserList_data @arguments(input: $input, after: $after, first: $first)
    }
`;
