import throttle from 'lodash/throttle';
import React from 'react';
import injectSheet, { KeyFrames, Styles } from 'react-jss';
import { createPaginationContainer, graphql, RelayPaginationProp } from 'react-relay';
import { Theme } from 'theme';
import { getDocHeight } from 'utils';
import InfoPlaceholder from '../CampaignInfoPlaceholder';
import { UserListQuery } from './gql';
import UserDetails, { User } from './UserDetails';

interface Node {
    node: User;
}

interface UserListProps {
    data: {
        userMany?: {
            edges: Node[];
        };
    };
    relay: RelayPaginationProp;
    classes: Record<ClassNames, string>;
}

type ClassNames = 'loading' | 'root';

type keyframes = '@keyframes fadeIn';

const styles = (theme: Theme): Styles<ClassNames> & KeyFrames<keyframes> => ({
    '@keyframes fadeIn': {
        '0%': {
            opacity: 0,
        },
        '100%': {
            opacity: 1,
        },
    },
    loading: {
        display: 'flex',
        height: 150,
        justifyContent: 'center',
        margin: '25px auto',
        maxWidth: 500,
        width: '80%',
    },
    root: {
        animation: {
            duration: theme.transitions.duration.complex,
            name: 'fadeIn',
            timingFunction: theme.transitions.easing.easeIn,
        },
    },
});

interface UserListState {
    isLoading: boolean;
}

class UserList extends React.Component<UserListProps, UserListState> {
    constructor(props: UserListProps) {
        super(props);
        this.state = {
            isLoading: props.relay.isLoading(),
        };
    }
    private _loadNext = throttle(() => {
        const { isLoading, loadMore, hasMore } = this.props.relay;
        if (isLoading() || !hasMore()) {
            return;
        }
        const docHeight = getDocHeight();
        const winHeight = window.innerHeight || (document.documentElement || document.body).clientHeight;
        if (docHeight - winHeight - 20 < window.pageYOffset) {
            this.setState({ isLoading: true }, () => {
                loadMore(2, () => {
                    this.setState({ isLoading: false });
                });
            });
        }
    }, 160);

    public componentDidMount() {
        window.addEventListener('scroll', this._loadNext);
    }

    public componentWillUnmount() {
        this._loadNext.cancel();
        window.removeEventListener('scroll', this._loadNext);
    }
    public render() {
        const { isLoading } = this.state;
        const { userMany } = this.props.data;
        const { hasMore } = this.props.relay;
        const { loading, root } = this.props.classes;
        if (userMany) {
            return (
                <div className={root}>
                    {userMany.edges.map((node: Node, index: number) => <UserDetails key={index} user={node.node} />)}
                    {isLoading && hasMore() ? (
                        <div className={loading}>
                            <InfoPlaceholder />
                        </div>
                    ) : null}
                </div>
            );
        }
        return null;
    }
}

export default createPaginationContainer(
    injectSheet(styles)(UserList),
    {
        data: graphql`
            fragment UserList_data on RootQuery
                @argumentDefinitions(
                    first: { type: "Int" }
                    after: { type: "String" }
                    input: { type: "UserListInput!" }
                ) {
                userMany(input: $input, after: $after, first: $first) @connection(key: "UserList_userMany") {
                    edges {
                        node {
                            ...UserDetails_user
                        }
                    }
                    pageInfo {
                        hasNextPage
                        hasPreviousPage
                        endCursor
                        startCursor
                    }
                }
            }
        `,
    },
    {
        direction: 'forward',
        getConnectionFromProps: props => props.data.userMany,
        getVariables: (_props, { cursor }, fragmentVariables) => {
            return {
                ...fragmentVariables,
                after: cursor,
            };
        },
        query: UserListQuery,
    }
);
