import { faAddressCard, faBuilding, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import injectStyle, { Styles } from 'react-jss';
import { createFragmentContainer, graphql } from 'react-relay';
import { Theme } from 'theme';

export interface User {
    email: string;
    firstName: string;
    lastName: string;
    company: {
        name: string;
    };
}

interface UserProps {
    user: User;
    classes: Record<ClassNames, string>;
}

type ClassNames = 'body' | 'bodyText' | 'group' | 'root' | 'icon' | 'title' | 'userName';

const style = (theme: Theme): Styles<ClassNames> => ({
    body: {
        alignItems: 'start',
        extend: 'title',
        flexDirection: 'column',
        ...theme.typography.body1,
    },
    bodyText: {
        paddingLeft: 10,
        textTransform: 'uppercase',
    },
    group: {
        alignItems: 'center',
        display: 'flex',
        paddingBottom: 10,
        paddingTop: 10,
    },
    icon: {
        color: theme.palette.action.active,
    },
    root: {
        backgroundColor: theme.palette.background.paper,
        border: theme.borders.default,
        margin: '25px auto',
        maxWidth: 500,
        width: '80%',
    },
    title: {
        alignItems: 'center',
        display: 'flex',
        padding: 15,
    },
    userName: {
        ...theme.typography.title,
        paddingLeft: 15,
    },
});

const UserDetails = (props: UserProps) => {
    const { email, firstName, lastName, company } = props.user;
    const { body, bodyText, group, icon, root, title, userName } = props.classes;
    return (
        <div className={root}>
            <div className={title}>
                <FontAwesomeIcon className={icon} icon={faAddressCard} size={'3x'} />
                <span className={userName}>
                    {firstName} {lastName}
                </span>
            </div>
            <div className={body}>
                <span>Details:</span>
                <div className={group}>
                    <FontAwesomeIcon className={icon} icon={faBuilding} size={'2x'} />
                    <span className={bodyText}>{company.name}</span>
                </div>
                <div className={group}>
                    <FontAwesomeIcon className={icon} icon={faEnvelope} size={'2x'} />
                    <span className={bodyText}>{email}</span>
                </div>
            </div>
        </div>
    );
};

export default createFragmentContainer(injectStyle(style)(UserDetails), {
    user: graphql`
        fragment UserDetails_user on User {
            firstName
            lastName
            email
            company {
                name
            }
        }
    `,
});
