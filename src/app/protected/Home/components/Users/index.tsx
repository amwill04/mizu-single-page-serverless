import DebouncedInput from '../../../../../Components/DebouncedInput/index';
import { environment } from 'graphql';
import React from 'react';
import injectSheet, { Styles } from 'react-jss';
import { QueryRenderer, ReadyState } from 'react-relay';
import InfoPlaceholder from '../CampaignInfoPlaceholder';
import { UserListQuery } from './gql/index';
import UserList from './UserList';

interface UsersProps {
    classes: Record<ClassNames, string>;
}

interface UsersState {
    firstName: string;
    lastName: string;
}

type ClassNames = 'search' | 'searchGroup';

const styles: Styles<ClassNames> = {
    search: {
        width: 500,
    },
    searchGroup: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
        margin: '0px auto',
    },
};
const renderUserList = ({ error, props }: ReadyState) => {
    if (error) {
        return <div>Error</div>;
    }
    if (props) {
        return <UserList data={props} />;
    }
    return (
        <div
            style={{
                display: 'flex',
                height: 150,
                justifyContent: 'center',
                margin: '25px auto',
                maxWidth: 500,
                width: '80%',
            }}
        >
            <InfoPlaceholder />
        </div>
    );
};

class Users extends React.Component<UsersProps, UsersState> {
    constructor(props: UsersProps) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
        };
    }

    private _onChangeFirstName = (event: Event) => {
        event.preventDefault();
        this.setState({ firstName: (event.target as HTMLInputElement).value });
    };

    private _onChangeLastName = (event: Event) => {
        event.preventDefault();
        this.setState({ lastName: (event.target as HTMLInputElement).value });
    };

    public render() {
        const { firstName, lastName } = this.state;
        const { search, searchGroup } = this.props.classes;
        return (
            <div>
                <div className={searchGroup}>
                    <div className={search}>
                        <DebouncedInput
                            debounceTimeout={500}
                            onChange={this._onChangeFirstName}
                            placeholder={'First Name'}
                        />
                    </div>
                    <div className={search}>
                        <DebouncedInput
                            debounceTimeout={500}
                            onChange={this._onChangeLastName}
                            placeholder={'Last Name'}
                        />
                    </div>
                </div>
                <QueryRenderer
                    environment={environment}
                    render={renderUserList}
                    variables={{ input: { firstName, lastName }, first: 2, after: null }}
                    query={UserListQuery}
                />
            </div>
        );
    }
}

export default injectSheet(styles)(Users);
