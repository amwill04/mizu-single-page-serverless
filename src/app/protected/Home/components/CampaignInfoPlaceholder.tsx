import React from 'react';
import injectSheet, { Styles } from 'react-jss';
import { Theme } from 'theme';

type classNames = 'group' | 'rect';

interface PlaceholderProps {
    classes: Record<classNames, string>;
    width?: number;
    height?: number;
}

const styles = (theme: Theme): Styles<classNames> => ({
    group: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        justifyContent: 'center',
    },
    rect: {
        fill: theme.palette.colors.grey['150'],
    },
});

const InfoPlaceholder: React.SFC<PlaceholderProps> = ({ classes: { group, rect }, height, width }) => (
    <div className={group}>
        <svg
            version={'1.1'}
            id={'Layer_1'}
            xmlns={'http://www.w3.org/2000/svg'}
            xmlnsXlink={'http://www.w3.org/1999/xlink'}
            x={0}
            y={0}
            width={`${width}px`}
            height={`${height}px`}
            viewBox={'0 0 24 30'}
            xmlSpace={'preserve'}
        >
            <rect className={rect} x="0" y="8.55555" width="4" height="13.8889" fill="#333">
                <animate
                    attributeName="height"
                    attributeType="XML"
                    values="5;21;5"
                    begin="0s"
                    dur="1s"
                    repeatCount="indefinite"
                />
                <animate
                    attributeName="y"
                    attributeType="XML"
                    values="13; 5; 13"
                    begin="0s"
                    dur="1s"
                    repeatCount="indefinite"
                />
            </rect>
            <rect className={rect} x="10" y="5.44445" width="4" height="20.1111" fill="#333">
                <animate
                    attributeName="height"
                    attributeType="XML"
                    values="5;21;5"
                    begin="0.15s"
                    dur="1s"
                    repeatCount="indefinite"
                />
                <animate
                    attributeName="y"
                    attributeType="XML"
                    values="13; 5; 13"
                    begin="0.15s"
                    dur="1s"
                    repeatCount="indefinite"
                />
            </rect>
            <rect className={rect} x="20" y="9.44445" width="4" height="12.1111" fill="#333">
                <animate
                    attributeName="height"
                    attributeType="XML"
                    values="5;21;5"
                    begin="0.3s"
                    dur="1s"
                    repeatCount="indefinite"
                />
                <animate
                    attributeName="y"
                    attributeType="XML"
                    values="13; 5; 13"
                    begin="0.3s"
                    dur="1s"
                    repeatCount="indefinite"
                />
            </rect>
        </svg>
    </div>
);

InfoPlaceholder.defaultProps = {
    height: 30,
    width: 24,
};

export default injectSheet(styles)(InfoPlaceholder);
