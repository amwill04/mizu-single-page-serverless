import { faBoxes, faClipboardCheck, faClipboardList, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import injectSheet, { KeyFrames, Styles } from 'react-jss';
import { Theme } from 'theme';
import InfoPlaceholder from './CampaignInfoPlaceholder';

type classNames = 'group' | 'groupHeader' | 'groupText' | 'groupNumber' | 'groupIcon';

interface InfoProps {
    classes: Record<classNames, string>;
    value: number;
    type: string;
}

interface IconInfo {
    icon: IconDefinition;
    text: string;
}

const styles = (theme: Theme): Styles<classNames> & KeyFrames<'@keyframes fadeIn'> => ({
    '@keyframes fadeIn': {
        '0%': {
            opacity: 0,
        },
        '100%': {
            opacity: 1,
        },
    },
    group: {
        animation: {
            duration: theme.transitions.duration.complex,
            name: 'fadeIn',
            timingFunction: theme.transitions.easing.easeIn,
        },
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        justifyContent: 'center',
    },
    groupHeader: {
        alignItems: 'center',
        display: 'flex',
    },
    groupIcon: {
        color: theme.palette.text.primary,
    },
    groupNumber: {
        ...theme.typography.display1,
    },
    groupText: {
        ...theme.typography.title,
        paddingLeft: 5,
    },
});

const getIconTitle = (type: string): IconInfo => {
    switch (type) {
        case 'completed': {
            return {
                icon: faClipboardCheck,
                text: 'Completed',
            };
        }
        case 'pending': {
            return {
                icon: faClipboardList,
                text: 'Pending',
            };
        }
        case 'total': {
            return {
                icon: faBoxes,
                text: 'Total',
            };
        }
        default: {
            return {
                icon: faClipboardCheck,
                text: 'Completed',
            };
        }
    }
};

const CampaignInfo: React.SFC<InfoProps> = ({ classes, value, type }) => {
    const { group, groupHeader, groupText, groupNumber, groupIcon } = classes;
    const { icon, text } = getIconTitle(type);
    if (!value) {
        return <InfoPlaceholder />;
    }
    return (
        <div className={group}>
            <div className={groupHeader}>
                <span>
                    <FontAwesomeIcon className={groupIcon} icon={icon} size={'lg'} />
                </span>
                <span className={groupText}>{text}</span>
            </div>
            <span className={groupNumber}>{value}</span>
        </div>
    );
};

export default injectSheet(styles)(CampaignInfo);
