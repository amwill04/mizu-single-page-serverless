import Loading from 'loading';
import React from 'react';
import Loadable, { LoadingComponentProps } from 'react-loadable';

const Landing = Loadable({
    loader: () => import('./Home.Component'),
    loading(props: LoadingComponentProps) {
        if (props.pastDelay) {
            return <Loading />;
        }
        return null;
    },
});

export default Landing;
