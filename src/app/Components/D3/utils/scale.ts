import * as D3Scale from 'd3-scale';

enum scaleIdentites {
    band = 'band',
    linear = 'linear',
    time = 'time',
}

interface BandProps {
    scale: 'band';
    domain: number[] | string[];
    range: [number, number];
    padding?: number;
}

interface LinearProps {
    scale: 'linear';
    domain: number[];
    range: [number, number];
    padding?: number;
}
interface TimeProps {
    scale: 'time';
    domain: Date[];
    range: [number, number];
    padding?: number;
}

export function scale(props: BandProps): D3Scale.ScaleBand<number> | D3Scale.ScaleBand<string>;
export function scale(props: LinearProps): D3Scale.ScaleLinear<number, number>;
export function scale(props: TimeProps): D3Scale.ScaleTime<number, number>;
/*tslint:disable:no-shadowed-variable*/
export function scale({
    scale,
    range,
    domain,
    padding,
}: BandProps | LinearProps | TimeProps):
    | D3Scale.ScaleBand<number>
    | D3Scale.ScaleBand<string>
    | D3Scale.ScaleLinear<number, number>
    | D3Scale.ScaleTime<number, number> {
    switch (scale) {
        case scaleIdentites.band: {
            if (typeof domain[0] === 'number') {
                const scaleFunctionNumber = D3Scale.scaleBand<number>();
                scaleFunctionNumber.domain(domain as number[]);
                scaleFunctionNumber.range(range as [number, number]);
                scaleFunctionNumber.round(true);
                scaleFunctionNumber.padding(padding || 0.1);
                return scaleFunctionNumber;
            } else {
                const scaleFunctionNumber = D3Scale.scaleBand<string>();
                scaleFunctionNumber.domain(domain as string[]);
                scaleFunctionNumber.range(range as [number, number]);
                scaleFunctionNumber.round(true);
                scaleFunctionNumber.padding(padding || 0.1);
                return scaleFunctionNumber;
            }
        }
        case scaleIdentites.linear: {
            const scaleFunction = D3Scale.scaleLinear();
            scaleFunction.domain(domain as Array<number | { valueOf(): number }>);
            scaleFunction.nice();
            scaleFunction.range(range as [number, number]);
            return scaleFunction;
        }
        case scaleIdentites.time: {
            const scaleFunction = D3Scale.scaleTime();
            if (range) {
                scaleFunction.range(range as [number, number]);
            }
            if (domain) {
                scaleFunction.domain(domain as Date[]);
            }
            return scaleFunction;
        }
        default: {
            throw new Error(`Unknow scale ${scale}`);
        }
    }
}
