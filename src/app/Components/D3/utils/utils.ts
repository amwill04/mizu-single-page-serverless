import { Omit } from 'utility-types';
import { CommonProps } from '../core/@types/Props';

export const calcChartDimensions = ({ height, width, margin }: Omit<CommonProps, 'data'>) => {
    const chartHeight = height - margin.bottom - margin.top;
    const chartWidth = width - margin.left - margin.right;
    return {
        height: chartHeight,
        width: chartWidth,
    };
};
