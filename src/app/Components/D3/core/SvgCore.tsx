import React from 'react';
import { Context } from './@types/ChartProps';
import injectContext from './Context';

class SvgCore extends React.Component<Pick<Context, 'svg'>, {}> {
    public render() {
        const { svg } = this.props;
        return (
            <svg height={svg.height} width={svg.width}>
                {this.props.children}
            </svg>
        );
    }
}

const SvgCoreContext = injectContext(SvgCore);

export default SvgCoreContext;
