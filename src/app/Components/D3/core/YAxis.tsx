import React from 'react';
import { Context } from './@types/ChartProps';
import Axis from './Axis';
import injectContext from './Context';

export class YAxis extends React.Component<Pick<Context, 'y'>> {
    public static defaultProps = {
        tickOrientation: 'bottom',
    };
    public render() {
        return <Axis {...this.props.y} />;
    }
}

const XAxisContext = injectContext(YAxis);

export default XAxisContext;
