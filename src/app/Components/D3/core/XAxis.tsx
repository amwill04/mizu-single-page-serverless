import React from 'react';
import { Context } from './@types/ChartProps';
import Axis from './Axis';
import injectContext from './Context';

export class XAxis extends React.Component<Pick<Context, 'x'>> {
    public static defaultProps = {
        tickOrientation: 'bottom',
    };
    public render() {
        return <Axis {...this.props.x} />;
    }
}

const XAxisContext = injectContext(XAxis);

export default XAxisContext;
