import React from 'react';

const { Provider, Consumer } = React.createContext<any>({});

function injectContext(WrappedComponent: React.ComponentType): React.ComponentType<{}> {
    return class extends React.Component<{}> {
        public render() {
            return (
                <Consumer>
                    {props => <WrappedComponent {...props.settings}>{this.props.children}</WrappedComponent>}
                </Consumer>
            );
        }
    };
}

export default injectContext;
export { Provider, Consumer };
