import React from 'react';
import { Context } from './@types/ChartProps';
import { Provider } from './Context';
import SvgCore from './SvgCore';
import XAxis from './XAxis';
import YAxis from './YAxis';

class Chart extends React.Component<{ settings: Context }, {}> {
    public render() {
        return (
            <Provider value={this.props}>
                <SvgCore>
                    <XAxis />
                    <YAxis />
                    {this.props.children}
                </SvgCore>
            </Provider>
        );
    }
}

export default Chart;
