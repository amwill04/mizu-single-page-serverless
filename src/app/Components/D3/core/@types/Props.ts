export interface CommonProps {
    height: number;
    width: number;
    margin: Record<'bottom' | 'left' | 'top' | 'right', number>;
    data: Array<{ [k: string]: any }>;
}

export interface TransitionProps {
    duration: number;
}
