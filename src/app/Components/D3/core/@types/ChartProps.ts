import * as D3Axis from 'd3-axis';
import * as D3Scale from 'd3-scale';

interface Ticks {
    countOrInterval: number | D3Axis.AxisTimeInterval;
    specifier?: string;
}

type CustomTickFormat = (domain: Date[] | number[] | string[], index: number) => string;

type D3TickFormat = (n: number | { valueOf(): number }) => string;

export interface AxisStyleProps {
    className?: string;
    padding?: number;
    tickFormat?: CustomTickFormat | D3TickFormat;
    tickOrientation?: 'bottom' | 'right' | 'left' | 'top';
    tickPadding?: number;
    tickSizeOuter?: number;
    tickSizeInner?: number;
    tickValues?: Date[] | number[] | string[];
    ticks?: Ticks;
    min?: number;
    max?: number;
}

export interface AxisProps extends AxisStyleProps {
    domain: number[] | Date[] | string[];
    range: [number, number];
    scale: D3Scale.ScaleLinear<any, any> | D3Scale.ScaleBand<any> | D3Scale.ScaleTime<any, any>;
    translation: [number, number];
}

interface Dimensions {
    height: number;
    width: number;
}

export interface Context {
    svg: Dimensions;
    chartArea: Dimensions;
    x: AxisProps;
    y: AxisProps;
}
