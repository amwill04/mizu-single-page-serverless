import * as D3Axis from 'd3-axis';
import * as D3Selection from 'd3-selection';
import React from 'react';
import injectSheet, { InjectedClasses, Styles } from 'react-jss';
import { Theme } from 'theme';
import { AxisProps } from './@types/ChartProps';

const styles = (theme: Theme): Styles<'axis'> => ({
    axis: {
        '& line': {
            stroke: theme.palette.colors.grey[200],
        },
        '& path': {
            stroke: theme.palette.colors.grey[200],
        },
        '& text': {
            fill: theme.palette.colors.grey[200],
            fontFamily: theme.typography.fontFamily,
            fontSize: theme.typography.fontSize,
        },
    },
});

class Axis extends React.Component<AxisProps & InjectedClasses<'axis'>, {}> {
    constructor(props: AxisProps & InjectedClasses<'axis'>) {
        super(props);
        this.axisRef = React.createRef();
    }

    private readonly axisRef: React.RefObject<SVGGElement>;

    private _makeTicks(): any {
        const {
            scale,
            tickOrientation,
            tickFormat,
            tickPadding,
            tickSizeOuter,
            tickSizeInner,
            tickValues,
            ticks,
        } = this.props;
        let axis;
        switch (tickOrientation) {
            case 'bottom':
                axis = D3Axis.axisBottom<any>(scale);
                break;
            case 'top':
                axis = D3Axis.axisTop<any>(scale);
                break;
            case 'left':
                axis = D3Axis.axisLeft<any>(scale);
                break;
            case 'right':
                axis = D3Axis.axisRight<any>(scale);
                break;
            default:
                throw Error(`Unknwon tick orientation ${tickOrientation}`);
        }
        if (tickFormat) {
            axis.tickFormat(tickFormat);
        }
        if (tickPadding) {
            axis.tickPadding(tickPadding);
        }
        if (typeof tickSizeOuter === 'number') {
            axis.tickSizeOuter(tickSizeOuter);
        }
        if (typeof tickSizeInner === 'number') {
            axis.tickSizeInner(tickSizeInner);
        }
        if (tickValues) {
            axis.tickValues(tickValues);
        }
        if (ticks) {
            axis.ticks(ticks.countOrInterval, ticks.specifier);
        }
        return axis;
    }

    private _makeAxis() {
        const { classes, className } = this.props;
        const axisDom = D3Selection.select(this.axisRef.current);
        axisDom.attr('class', className || classes.axis);
        axisDom.call(this._makeTicks());
    }

    public componentDidMount() {
        this._makeAxis();
    }

    public render() {
        const { translation } = this.props;
        return <g ref={this.axisRef} transform={`translate(${translation[0]}, ${translation[1]})`} />;
    }
}

export default injectSheet(styles)(Axis);
