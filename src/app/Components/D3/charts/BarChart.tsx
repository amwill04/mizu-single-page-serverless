import * as D3Array from 'd3-array';
import React from 'react';
import { Chart, CommonProps, TransitionProps, XAxis } from '../core';
import { AxisStyleProps } from '../core/@types/ChartProps';
import { Bar } from '../shapes';
import { scale } from '../utils/scale';
import { calcChartDimensions } from '../utils/utils';

interface BarProps extends CommonProps {
    xValues?: Date[] | number[] | string[] | string;
    yValues?: [number, number] | string;
    xProps: AxisStyleProps;
    yProps: AxisStyleProps;
}

class BarChart extends React.Component<BarProps & TransitionProps, {}> {
    private _calcDomain() {
        const { xValues, yValues, data, yProps } = this.props;
        const xDomain = !xValues
            ? data.map((datum: any) => datum.key)
            : typeof xValues === 'string'
                ? data.map((datum: any) => datum[xValues])
                : xValues;
        const yValueArray = !yValues
            ? data.map((datum: any) => datum.value)
            : typeof yValues === 'string'
                ? data.map((datum: any) => datum[yValues])
                : yValues;
        const yMin = D3Array.min([0, yProps.min, ...yValueArray]);
        return {
            xDomain,
            yDomain: [yMin, D3Array.max([yProps.max, ...yValueArray])],
        };
    }

    public static defaultProps = {
        duration: 750,
        height: 400,
        margin: { top: 20, right: 20, bottom: 30, left: 40 },
        width: 960,
        xLabel: 'value',
        xProps: { tickOrientation: 'bottom' },
        yLabel: 'key',
        yProps: {},
    };

    public render() {
        const { data, duration, height, margin, width, xProps, xValues, yProps, yValues } = this.props;
        const { xDomain, yDomain } = this._calcDomain();
        const dimensions = calcChartDimensions(this.props);
        const xScale = scale({ scale: 'band', domain: xDomain, range: [0, dimensions.width], padding: xProps.padding });
        const yScale = scale({
            domain: yDomain,
            padding: yProps.padding,
            range: [dimensions.height, 0],
            scale: 'linear',
        });
        const hasNegatives = D3Array.min(yDomain) < 0;
        const BarSettings = {
            data,
            duration,
            hasNegatives,
            horizontal: false,
            translation: [margin.left, margin.top] as [number, number],
            xScale,
            xValues: typeof xValues === 'string' ? xValues : 'key',
            yScale,
            yValues: typeof yValues === 'string' ? yValues : 'value',
        };
        return (
            <Chart
                settings={{
                    chartArea: dimensions,
                    svg: {
                        height,
                        width,
                    },
                    x: {
                        domain: xDomain,
                        range: [0, dimensions.width],
                        scale: xScale,
                        tickOrientation: xProps.tickOrientation || 'bottom',
                        translation: [margin.left, hasNegatives ? yScale(0) + margin.top : height - margin.bottom],
                        ...(hasNegatives ? { tickValues: [], tickSizeOuter: 0 } : {}),
                        ...xProps,
                    },
                    y: {
                        domain: yDomain,
                        range: [dimensions.height, 0],
                        scale: yScale,
                        tickOrientation: yProps.tickOrientation || 'left',
                        translation: [margin.left, margin.top],
                        ...yProps,
                    },
                }}
            >
                {hasNegatives ? (
                    <XAxis
                        x={{
                            domain: xDomain,
                            range: [0, dimensions.width],
                            scale: xScale,
                            tickOrientation: xProps.tickOrientation || 'bottom',
                            translation: [margin.left, height - margin.bottom],
                            ...xProps,
                        }}
                    />
                ) : null}
                <Bar {...BarSettings} />
            </Chart>
        );
    }
}

export default BarChart;
