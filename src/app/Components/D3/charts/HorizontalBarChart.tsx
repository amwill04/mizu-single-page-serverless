import * as D3Array from 'd3-array';
import React from 'react';
import { Chart, CommonProps, TransitionProps, YAxis } from '../core';
import { AxisStyleProps } from '../core/@types/ChartProps';
import { Bar } from '../shapes';
import { scale } from '../utils/scale';
import { calcChartDimensions } from '../utils/utils';

interface HorizontalBarProps extends CommonProps {
    xValues?: Date[] | number[] | string[] | string;
    yValues?: [number, number] | string;
    xProps: AxisStyleProps;
    yProps: AxisStyleProps;
}

class HorizontalBarChart extends React.Component<HorizontalBarProps & TransitionProps, {}> {
    private _calcDomain() {
        const { xValues, yValues, data, xProps } = this.props;
        const yDomain = !yValues
            ? data.map((datum: any) => datum.key)
            : typeof yValues === 'string'
                ? data.map((datum: any) => datum[yValues])
                : yValues;
        const xValueArray = !xValues
            ? data.map((datum: any) => datum.value)
            : typeof xValues === 'string'
                ? data.map((datum: any) => datum[xValues])
                : xValues;
        const xMin = D3Array.min([0, xProps.min, ...xValueArray]);
        return {
            xDomain: [xMin, D3Array.max([xProps.max, ...xValueArray])],
            yDomain,
        };
    }

    public static defaultProps = {
        duration: 750,
        height: 400,
        margin: { top: 20, right: 20, bottom: 30, left: 100 },
        width: 960,
        xLabel: 'key',
        xProps: { tickOrientation: 'bottom' },
        yLabel: 'value',
        yProps: {},
    };

    public render() {
        const { data, duration, height, margin, width, xProps, xValues, yProps, yValues } = this.props;
        const { xDomain, yDomain } = this._calcDomain();
        const dimensions = calcChartDimensions(this.props);
        const xScale = scale({
            domain: xDomain,
            padding: xProps.padding,
            range: [0, dimensions.width],
            scale: 'linear',
        });
        const yScale = scale({
            domain: yDomain,
            padding: yProps.padding,
            range: [dimensions.height, 0],
            scale: 'band',
        });
        const hasNegatives = D3Array.min(xDomain) < 0;
        const BarSettings = {
            data,
            duration,
            hasNegatives,
            horizontal: true,
            translation: [margin.left, margin.top] as [number, number],
            xScale,
            xValues: typeof xValues === 'string' ? xValues : 'value',
            yScale,
            yValues: typeof yValues === 'string' ? yValues : 'key',
        };
        return (
            <Chart
                settings={{
                    chartArea: dimensions,
                    svg: {
                        height,
                        width,
                    },
                    x: {
                        domain: xDomain,
                        range: [0, dimensions.width],
                        scale: xScale,
                        tickOrientation: xProps.tickOrientation || 'bottom',
                        translation: [margin.left, height - margin.bottom],
                        ...xProps,
                    },
                    y: {
                        domain: yDomain,
                        range: [dimensions.height, 0],
                        scale: yScale,
                        tickOrientation: yProps.tickOrientation || 'left',
                        translation: [hasNegatives ? margin.left + xScale(0) : margin.left, margin.top],
                        ...(hasNegatives ? { tickValues: [], tickSizeOuter: 0 } : {}),
                        ...yProps,
                    },
                }}
            >
                {hasNegatives ? (
                    <YAxis
                        y={{
                            domain: yDomain,
                            range: [0, dimensions.width],
                            scale: yScale,
                            tickOrientation: xProps.tickOrientation || 'left',
                            translation: [margin.left, margin.top],
                            ...yProps,
                        }}
                    />
                ) : null}
                <Bar {...BarSettings} />
            </Chart>
        );
    }
}

export default HorizontalBarChart;
