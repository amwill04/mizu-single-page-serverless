import * as D3Scale from 'd3-scale';
import * as D3Selection from 'd3-selection';
import 'd3-transition';
import React from 'react';
import injectSheet, { InjectedClasses, Styles } from 'react-jss';
import { Theme } from 'theme';
import { CommonProps, TransitionProps } from '../core';

interface BarProps extends Pick<CommonProps, 'data'> {
    className?: string;
    horizontal: boolean;
    translation: [number, number];
    hasNegatives: boolean;
    xValues: string;
    yValues: string;
    xScale:
        | D3Scale.ScaleBand<number>
        | D3Scale.ScaleBand<string>
        | D3Scale.ScaleLinear<number, number>
        | D3Scale.ScaleTime<number, number>;
    yScale:
        | D3Scale.ScaleBand<number>
        | D3Scale.ScaleBand<string>
        | D3Scale.ScaleLinear<number, number>
        | D3Scale.ScaleTime<number, number>;
}

type ClassNames = 'barPositive' | 'barNegative';

const styles = (theme: Theme): Styles<ClassNames> => ({
    barNegative: {
        '&:hover': {
            fill: theme.palette.error.light,
        },
        fill: theme.palette.error.main,
    },
    barPositive: {
        '&:hover': {
            fill: theme.palette.primary.light,
        },
        fill: theme.palette.primary.main,
    },
});

class Bar extends React.Component<BarProps & InjectedClasses<ClassNames> & TransitionProps, {}> {
    constructor(props: BarProps & InjectedClasses<ClassNames> & TransitionProps) {
        super(props);
        this.barRef = React.createRef();
    }

    private readonly barRef: React.RefObject<SVGGElement>;

    private _makePositions() {
        const { horizontal, xValues, xScale, yValues, yScale, classes } = this.props;
        if (horizontal) {
            return {
                class: (datum: any) => {
                    if (datum[xValues] < 0) {
                        return classes.barNegative;
                    }
                    return classes.barPositive;
                },
                height: (yScale as D3Scale.ScaleBand<any>).bandwidth(),
                width: (datum: any) =>
                    Math.abs(
                        (xScale as D3Scale.ScaleLinear<any, any>)(datum[xValues]) -
                            (xScale as D3Scale.ScaleLinear<any, any>)(0)
                    ) as number,
                x: (datum: any) => (xScale as D3Scale.ScaleLinear<any, any>)(Math.min(0, datum[xValues])),
                y: (datum: any) => (yScale as D3Scale.ScaleBand<any>)(datum[yValues]) as number,
            };
        } else {
            return {
                class: (datum: any) => {
                    if (datum[yValues] < 0) {
                        return classes.barNegative;
                    }
                    return classes.barPositive;
                },
                height: (datum: any) =>
                    Math.abs(
                        (yScale as D3Scale.ScaleLinear<any, any>)(datum[yValues]) -
                            (yScale as D3Scale.ScaleLinear<any, any>)(0)
                    ),
                width: (xScale as D3Scale.ScaleBand<any>).bandwidth(),
                x: (datum: any) => (xScale as D3Scale.ScaleBand<any>)(datum[xValues]) as number,
                y: (datum: any) => {
                    if (datum[yValues] > 0) {
                        return (yScale as D3Scale.ScaleLinear<any, any>)(datum[yValues]);
                    } else {
                        return (yScale as D3Scale.ScaleLinear<any, any>)(0);
                    }
                },
            };
        }
    }

    private _makeVerticalBars() {
        const { data, horizontal, hasNegatives, duration, xScale, yScale } = this.props;
        const barDom = D3Selection.select(this.barRef.current);
        const positions = this._makePositions();
        const bars = barDom
            .selectAll('.bar')
            .data(data)
            .enter()
            .append('rect');
        if (horizontal) {
            bars.attr('class', positions.class)
                .attr('y', positions.y)
                .attr('height', positions.height as number)
                .attr('x', () => (hasNegatives ? (xScale as D3Scale.ScaleLinear<any, any>)(0) : 0))
                .transition()
                .duration(duration)
                .attr('x', positions.x)
                .attr('width', positions.width as number);
        } else {
            bars.attr('class', positions.class)
                .attr('y', () => (hasNegatives ? (yScale as D3Scale.ScaleLinear<any, any>)(0) : 0))
                .attr('x', positions.x)
                .attr('width', positions.width as number)
                .transition()
                .duration(duration)
                .attr('y', positions.y)
                .attr('height', positions.height as number);
        }
    }

    public static defaultProps = {
        horizontal: false,
        xValues: 'key',
        yValues: 'value',
    };

    public componentDidMount() {
        this._makeVerticalBars();
    }

    public render() {
        const { translation } = this.props;
        return <g ref={this.barRef} transform={`translate(${translation[0]}, ${translation[1]})`} />;
    }
}

export default injectSheet(styles)(Bar);
