import React from 'react';
import injectSheet, { InjectedClasses, Styles } from 'react-jss';
import { Theme } from 'theme';
import Logout from './Components/Logout';

type ClassNames = 'root';

type HeaderProps = InjectedClasses<ClassNames>;

const styles = (theme: Theme): Styles<ClassNames> => ({
    root: {
        color: theme.palette.background.default,
        height: 50,
        width: '100%',
    },
});

class Header extends React.Component<HeaderProps, {}> {
    public render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <Logout />
            </div>
        );
    }
}

export default injectSheet(styles)(Header);
