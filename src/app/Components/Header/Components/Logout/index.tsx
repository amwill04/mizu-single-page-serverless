import React, { SyntheticEvent } from 'react';
import { fetchPage } from 'utils/fetch';
import Logout from './Logout.Component';

export default () => {
    const handleLogout = (event: SyntheticEvent<HTMLDivElement>): void => {
        event.preventDefault();
        const endpoint = process.env.NODE_ENV === 'development' ? 'http://127.0.0.1:3001/logout' : '/logout';
        fetchPage(endpoint)
            .then(() => {
                document.location.href = '/';
            })
            .catch((error: string) => {
                console.log(error);
            });
    };

    return <Logout onClick={handleLogout} />;
};
