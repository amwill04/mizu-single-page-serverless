import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { SyntheticEvent } from 'react';
import injectSheet, { Styles } from 'react-jss';
import { Theme } from 'theme';

type ClassNames = 'root' | 'icon';

interface LogoutProps {
    classes: Record<ClassNames, string>;
    onClick: (E: SyntheticEvent<HTMLDivElement>) => void;
}

const styles = (theme: Theme): Styles<ClassNames> => ({
    icon: {
        color: theme.palette.action.active,
    },
    root: {
        '&:hover': {
            transform: 'scale(1.1)',
        },
        alignItems: 'center',
        cursor: 'pointer',
        display: 'flex',
        float: 'right',
        height: '100%',
        justifyContent: 'center',
        width: 50,
    },
});

class Logout extends React.Component<LogoutProps, {}> {
    constructor(props: LogoutProps) {
        super(props);
    }

    public render() {
        const { classes: { icon, root }, onClick } = this.props;
        return (
            <div className={root} onClick={onClick}>
                <FontAwesomeIcon className={icon} icon={faSignOutAlt} size={'lg'} />
            </div>
        );
    }
}

export default injectSheet(styles)(Logout);
