import React from 'react';
import LandingComponent, { LandingProps } from './Landing.Component';

type P = Pick<LandingProps, Exclude<keyof LandingProps, 'classes'>>;

const Landing: React.StatelessComponent<P> = (props: P) => <LandingComponent {...props} />;

export default Landing;
