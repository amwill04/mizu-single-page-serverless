import Loading from 'loading';
import React from 'react';
import Loadable, { LoadingComponentProps } from 'react-loadable';

const Landing = Loadable({
    loader: () => import(/* webpackChunkName: "Landing" */ './Landing'),
    loading(props: LoadingComponentProps) {
        if (props.pastDelay) {
            return <Loading />;
        }
        return null;
    },
});

export default Landing;
