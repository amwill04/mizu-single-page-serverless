import React from 'react';
import injectSheet from 'react-jss';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Theme } from 'theme';
import Background from './assets/background.jpg';

type ClassNames = 'root' | 'linkContainer' | 'link' | 'textRoot' | 'title' | 'subHeader';

export interface LandingProps extends RouteComponentProps<null> {
    classes: Record<ClassNames, string>;
}

/* tslint:disable:object-literal-sort-keys */
const styles = (theme: Theme) => ({
    root: {
        height: '100%',
        width: '100%',
        textAlign: 'center',
        background: {
            image: `url(${Background})`,
            repeat: 'no-repeat',
            position: 'center',
            size: 'cover',
        },
        animation: {
            name: 'fadeIn',
            duration: theme.transitions.duration.complex,
            timingFunction: theme.transitions.easing.easeIn,
        },
    },
    textRoot: {
        paddingTop: 30,
    },
    text: {
        textTransform: 'uppercase',
        margin: 0,
        padding: 0,
    },
    title: {
        extend: 'text',
        ...theme.typography.display3,
        color: theme.palette.common.white,
    },
    subHeader: {
        extend: 'text',
        letterSpacing: '0.5rem',
    },
    linkContainer: {
        width: '100%',
        height: '10%',
        maxHeight: 100,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'fixed',
        bottom: 10,
    },
    link: {
        extend: 'text',
        ...theme.typography.display1,
        color: theme.palette.common.white,
        letterSpacing: '0.5rem',
        textDecoration: 'none',
        '&:hover': {
            transform: 'scale(1.2)',
            transition: {
                property: 'all',
                duration: theme.transitions.duration.short,
            },
        },
    },
    '@keyframes fadeIn': {
        '0%': {
            opacity: 0,
        },
        '100%': {
            opacity: 1,
        },
    },
});
/* tslint:enable:object-literal-sort-keys */

class LandingComponent extends React.Component<LandingProps> {
    public render() {
        const { classes: { root, linkContainer, link, textRoot, title, subHeader } } = this.props;
        return (
            <div className={root}>
                <div className={textRoot}>
                    <span className={title}>氷</span>
                    <p className={subHeader}>Kōri</p>
                </div>
                <div className={linkContainer}>
                    <Link className={link} to={'/login'}>
                        login
                    </Link>
                </div>
            </div>
        );
    }
}

export default injectSheet(styles)(LandingComponent);
