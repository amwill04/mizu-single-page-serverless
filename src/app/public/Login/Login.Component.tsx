import { faEnvelopeOpen, faLock, faLongArrowAltRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Field, Form, FormikProps } from 'formik';
import React from 'react';
import injectSheet, { KeyFrames, Styles } from 'react-jss';
import { Theme } from 'theme/index';
import Background from './assets/background.jpg';
import MultyUser from './assets/multy-user.png';

export interface FormValues {
    email: string;
    password: string;
}

type ClassNames = 'error' | 'input' | 'form' | 'legend' | 'span' | 'spanFocus' | 'submit' | 'root';

type keyframes = '@keyframes fadeIn';

export interface FormProps extends FormikProps<FormValues> {
    classes: Record<ClassNames, string>;
}

interface LoginState {
    emailFocus: boolean;
    passwordFocus: boolean;
}

const styles = (theme: Theme): Styles<ClassNames> & KeyFrames<keyframes> => ({
    '@keyframes fadeIn': {
        '0%': {
            opacity: 0,
        },
        '100%': {
            opacity: 1,
        },
    },
    error: {
        ...theme.typography.caption,
        color: theme.palette.error.light,
        textAlign: 'center',
    },
    form: {
        background: theme.palette.background.paper,
        borderRadius: 4,
        flex: {
            direction: 'column',
        },
        height: 240,
        position: 'relative',
        width: 250,
    },
    input: {
        '& input': {
            '&:focus': {
                borderColor: theme.palette.primary.light,
                outline: 0,
                padding: [10, 5, 10, 10],
            },
            border: theme.borders.lightSoft,
            color: theme.palette.text.hint,
            display: 'block',
            fontSize: theme.typography.pxToRem(12),
            padding: [10, 5, 10, 40],
            transition: {
                duration: 200,
                timeingFunction: theme.transitions.easing.easeOut,
            },
            width: '100%',
        },
        margin: '15px auto',
        position: 'relative',
        width: '90%',
    },
    legend: {
        ...theme.typography.title,
        '&:after': {
            backgroundImage: `url(${MultyUser})`,
            backgroundPosition: [152, -16],
            backgroundRepeat: 'no-repeat',
            backgroundSize: [100, 100],
            bottom: 0,
            content: '""',
            left: 0,
            opacity: 0.06,
            position: 'absolute',
            right: 0,
            top: 0,
        },
        background: theme.palette.primary.light,
        color: theme.palette.common.white,
        display: 'block',
        height: 50,
        padding: 10,
        position: 'relative',
        width: '100%',
    },
    root: {
        alignItems: 'center',
        animation: {
            duration: theme.transitions.duration.complex,
            name: 'fadeIn',
            timingFunction: theme.transitions.easing.easeIn,
        },
        background: {
            image: `url(${Background})`,
            position: 'center',
            repeat: 'no-repeat',
            size: 'cover',
        },
        display: 'flex',
        flex: {
            grow: 0,
            shrink: 0,
        },
        height: '100%',
        justifyContent: 'center',
        width: '100%',
    },
    span: {
        color: theme.palette.text.hint,
        display: 'block',
        fontSize: 20,
        left: 10,
        position: 'absolute',
        top: 6,
    },
    spanFocus: {
        extend: 'span',
        opacity: 0,
        transition: {
            duration: 200,
            property: 'opacity',
            timingFunction: theme.transitions.easing.easeOut,
        },
    },
    submit: {
        '&:hover': {
            background: theme.palette.primary.light,
            color: theme.palette.common.white,
            outline: 0,
        },
        background: theme.palette.common.white,
        border: theme.borders.createBorder(1, theme.palette.primary.light, 'solid', '100%'),
        bottom: -15,
        boxShadow: {
            blur: 0,
            color: theme.palette.common.white,
            spread: 7,
            x: 0,
            y: 0,
        },
        color: theme.palette.primary.light,
        cursor: 'pointer',
        display: 'block',
        fontSize: 24,
        height: 45,
        left: '50%',
        marginLeft: -22.5,
        position: 'absolute',
        transition: '0.2s ease-out',
        width: 45,
    },
});

class LoginComponent extends React.Component<FormProps, LoginState> {
    constructor(props: FormProps) {
        super(props);
        this.state = {
            emailFocus: false,
            passwordFocus: false,
        };
    }

    private handleFocusEvent = (event: React.FocusEvent<HTMLInputElement>): void => {
        if (event.currentTarget.getAttribute('name') === 'email') {
            this.setState((prevState: LoginState) => ({
                emailFocus: !prevState.emailFocus,
            }));
        } else {
            this.setState((prevState: LoginState) => ({
                passwordFocus: !prevState.passwordFocus,
            }));
        }
    };

    public render() {
        const { emailFocus, passwordFocus } = this.state;
        const {
            isSubmitting,
            classes: { error, form, legend, input, span, spanFocus, submit, root },
            status,
        } = this.props;
        return (
            <div className={root}>
                <Form className={form}>
                    <legend className={legend}>Login</legend>
                    <div className={input}>
                        <Field
                            type="email"
                            name="email"
                            placeholder={'Email'}
                            onFocus={this.handleFocusEvent}
                            onBlur={this.handleFocusEvent}
                        />
                        <span className={emailFocus ? spanFocus : span}>
                            <FontAwesomeIcon icon={faEnvelopeOpen} size={'sm'} />
                        </span>
                    </div>
                    <div className={input}>
                        <Field
                            type="password"
                            name="password"
                            placeholder="Password"
                            onFocus={this.handleFocusEvent}
                            onBlur={this.handleFocusEvent}
                        />
                        <span className={passwordFocus ? spanFocus : span}>
                            <FontAwesomeIcon icon={faLock} size={'sm'} />
                        </span>
                    </div>
                    {status ? <div className={error}>{status.message}</div> : null}
                    <button className={submit} type="submit" disabled={isSubmitting}>
                        <FontAwesomeIcon icon={faLongArrowAltRight} size={'sm'} />
                    </button>
                </Form>
            </div>
        );
    }
}

export default injectSheet(styles)(LoginComponent);
