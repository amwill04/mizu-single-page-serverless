// TODO Add 404 and 500 page
import { Formik, FormikActions } from 'formik';
import React from 'react';
import { fetchFormData } from 'utils/index';
import LoginComponent, { FormProps, FormValues } from './Login.Component';

interface LoginProps {
    initialEmail?: string;
}

const handleSubmit = async (values: FormValues, actions: FormikActions<FormValues>): Promise<void> => {
    try {
        const endpoint = process.env.NODE_ENV === 'development' ? 'http://127.0.0.1:3001/login' : '/login-submit';
        const response = await fetchFormData(endpoint, values);
        const data = await response.json();
        if (data.message) {
            actions.setStatus(data);
            return;
        }
        document.location.href = '/';
    } catch (e) {
        return;
    }
};

const renderForm = (props: FormProps) => <LoginComponent {...props} />;

const Login: React.StatelessComponent<LoginProps> = props => {
    return (
        <Formik
            initialValues={{ email: props.initialEmail || '', password: '' }}
            onSubmit={handleSubmit}
            render={renderForm}
        />
    );
};

export default Login;
