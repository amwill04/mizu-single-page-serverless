import React from 'react';
import { ThemeProvider } from 'react-jss';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import store from 'store';
import theme from '../theme/index';
import Landing from './public/Landing';
import Login from './public/Login/Login';

class App extends React.Component<{}, {}> {
    public render() {
        return (
            <ThemeProvider theme={theme}>
                <Provider store={store}>
                        <BrowserRouter>
                            <Switch>
                                <Route exact path="/" component={Landing} />
                                <Route path="/login" component={Login} />
                            </Switch>
                        </BrowserRouter>
                </Provider>
            </ThemeProvider>
        );
    }
}

export default App;
