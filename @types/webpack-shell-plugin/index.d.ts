declare module 'webpack-shell-plugin' {
    export default class WebpackShellPlugin {
        constructor(options: any);

        apply(compiler: any): void;

        handleScript(script: any): void;

        mergeOptions(options: any, defaults: any): any;

        puts(error: any, stdout: any, stderr: any): void;

        serializeScript(script: any): any;

        spreadStdoutAndStdErr(proc: any): void;

        validateInput(options: any): any;
    }
}
