declare module 'webpack-visualizer-plugin' {
    export default class WebpackVisualizerPlugin {
        constructor(...args: any[]);

        public apply(compiler: any): void;
    }
}
