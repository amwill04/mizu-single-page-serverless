import { StyleSheet, ToCssOptions } from './jss';

export declare class SheetsRegistry {
    protected registry: ReadonlyArray<StyleSheet>;
    protected index: number;

    public add(sheet: StyleSheet): void;

    public reset(): void;

    public remove(sheet: StyleSheet): void;

    public toString(options?: ToCssOptions): string;
}
