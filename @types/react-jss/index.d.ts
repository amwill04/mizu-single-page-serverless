declare module 'react-jss' {
    import * as React from 'react';
    import { Style } from './css';
    import { CreateStyleSheetOptions, Styles, StyleSheet } from './jss';
    import { SheetsRegistry } from './SheetsRegistry';

    export { StyleSheet, SheetsRegistry };
    export type Styles<Name extends string = any> = Record<Name, Style>;
    export type KeyFrames<f> = Record<f, Style>;
    interface ThemeProviderProps {
        theme?: object;
    }

    export declare const ThemeProvider: React.SFC<ThemeProviderProps>;

    export type ClassNameMap<ClassKey extends string = string> = Record<ClassKey, string>;

    export interface InjectedClasses<ClassKey extends string = string> {
        classes: ClassNameMap<ClassKey>;
    }

    function injectSheet<Name extends string>(
        styles: Partial<Styles<Name>>,
        options?: CreateStyleSheetOptions<Name>
    ): <P>(component: React.ComponentType<P>) => React.ComponentType<Pick<P, Exclude<keyof P, keyof InjectedClasses>>>;

    export default injectSheet;

    export const JssProvider: any;
}
