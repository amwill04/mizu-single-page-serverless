import Dotenv from 'dotenv-webpack';
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import fs from 'fs';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import path from 'path';
import webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
// import WebpackShellPlugin from 'webpack-shell-plugin';

const babelLoader = {
    loader: 'babel-loader',
    options: {
        cacheDirectory: true,
    },
};

const findDucks = (): any => {
    if (process.env.NODE_ENV === 'production') {
        return;
    }
    const rootPublic = path.resolve(__dirname, 'src/app/public');
    const dirsPublic = fs.readdirSync(rootPublic);
    if (dirsPublic.length === 0) {
        return {};
    }
    const publicDucks = dirsPublic.filter(dir => dir.indexOf('.') === -1).reduce((result: any, dir: any): any => {
        const dirs = fs.readdirSync(path.join(rootPublic, dir));
        const duckIndex = dirs.indexOf('duck');
        if (duckIndex !== -1) {
            result[`duck/public/${dir}`] = path.join(rootPublic, dir, 'duck');
            return result;
        }
        return result;
    }, {});

    const rootProtected = path.resolve(__dirname, 'src/app/protected');
    const dirsProtected = fs.readdirSync(rootProtected);
    if (dirsProtected.length === 0) {
        return {};
    }
    const protectedDucks = dirsProtected.filter(dir => dir.indexOf('.') === -1).reduce((result: any, dir: any): any => {
        const dirs = fs.readdirSync(path.join(rootProtected, dir));
        const duckIndex = dirs.indexOf('duck');
        if (duckIndex !== -1) {
            result[`duck/protected/${dir}`] = path.join(rootProtected, dir, 'duck');
            return result;
        }
        return result;
    }, {});

    return {...publicDucks, ...protectedDucks};

};

const config: webpack.Configuration & WebpackDevServer.Configuration = {
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        historyApiFallback: true,
        hot: true,
    },
    devtool: 'eval',
    entry: './src/index.tsx',
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [
                    babelLoader,
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: true,
                        },
                    },
                ],
            },
            {
                exclude: /node_modules/,
                test: /\.js$/,
                use: [babelLoader],
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]',
                    },
                },
            },
            {
                test: /\.mp4$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        mimetype: 'video/mp4',
                    },
                },
            },
        ],
    },
    optimization: {
        namedModules: true,
        noEmitOnErrors: true,
    },
    output: {
        filename: '[name].js',
        path: path.join(__dirname, 'dist'),
        publicPath: '/',
    },
    plugins: [
        new Dotenv(),
        // new WebpackShellPlugin({
        //     onBuildStart: ['yarn relay --watch'],
        // }),
        new webpack.HotModuleReplacementPlugin(),
        new ForkTsCheckerWebpackPlugin({
            checkSyntacticErrors: true,
            tslint: true,
            watch: ['./src'],
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            inject: 'body',
            template: path.join(__dirname, 'static/index.html'),
        }),
    ],
    resolve: {
        alias: {
            ...findDucks(),
            '@RootComponents': path.join(__dirname, 'src/Components'),
            Components: path.join(__dirname, 'src/app/Components'),
            graphql: path.join(__dirname, 'src/graphql'),
            loading: path.join(__dirname, 'src/Components/loading'),
            store: path.join(__dirname, 'src/store'),
            theme: path.join(__dirname, 'src/theme'),
        },
        extensions: ['.tsx', '.ts', '.js'],
        modules: ['src', 'node_modules'],
    },
    stats: {
        warningsFilter: /export .* was not found in/,
    },
};

export default config;
